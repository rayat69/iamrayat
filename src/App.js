import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import logo from "./logo.svg";
import "./App.css";

import Nav from "./shared/components/Nav/Nav";
import Home from './home/pages/Home'
import Projects from './projects/pages/Projects'
import MainNav from './shared/components/Navigation/MainNavigation'

function App() {
  return (
    <Router>
      <MainNav />
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/projects" exact>
          <Projects />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
