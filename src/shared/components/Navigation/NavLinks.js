import React from "react";
import { NavLink } from "react-router-dom";

import "./NavLinks.css";

const NavLinks = (props) => {
  return (
    <ul className="nav-links">
      <li onClick={props.onClick}>
        <NavLink to="/" exact>Home</NavLink>
      </li>
      <li onClick={props.onClick}>
        <NavLink to="/about">About</NavLink>
      </li>
      
      <li onClick={props.onClick}>
        <NavLink to="/resume">Resume</NavLink>
      </li>
      <li onClick={props.onClick}>
        <NavLink to="/projects">Projects</NavLink>
      </li>
    </ul>
  );
};

export default NavLinks;
