import React, { useState } from "react";
import { Link } from "react-router-dom";

import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import MainHeader from "./MainHeader";
import NavLinks from "./NavLinks";
import SideDrawer from "./SideDrawer";
import BackDrop from "./BackDrop";

import "./Nav.css";

const Nav = (props) => {
  const [drawerIsOpen, setDrawerIsOpen] = useState(false);

  const openDrawerHandler = () => {
    setDrawerIsOpen(true);
  };

  const closeDrawerHandler = () => {
    setDrawerIsOpen(false);
  };

  return (
    <React.Fragment>
      {drawerIsOpen && <BackDrop onClick={closeDrawerHandler} />}

      <SideDrawer show={drawerIsOpen}>
        <NavLinks />
      </SideDrawer>
      <MainHeader>
        <div className="menu-icon">
          <FontAwesomeIcon icon={faBars} />
        </div>
        <div className="logo">
          <Link to="/">My Profile</Link>
        </div>
        <NavLinks />
        <div className="cancel-icon">
          <FontAwesomeIcon icon={faTimes} />
        </div>
      </MainHeader>
      {/* <nav className="navbar sticky-top">
        <div className="menu-icon">
          <FontAwesomeIcon icon={faBars} />
        </div>
        <div className="logo">My Profile</div>
        <NavLinks />
        <div className="cancel-icon">
          <FontAwesomeIcon icon={faTimes} />
        </div>
      </nav> */}
    </React.Fragment>
  );
};

export default Nav;
