import React from "react";

import "./Shapes.css";

const Shapes = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className="hero-design"
      width="686"
      height="688"
      viewBox="0 0 686 688"
    >
      <g id="blockdesign" transform="translate(-935 -289)">
        <rect
          className="square-anim"
          data-name="Rectangle 2"
          rx="19"
          transform="translate(1277 289)"
          fill="#6e00ff"
        />
        <rect
          className="square-anim"
          data-name="Rectangle 10"
          rx="86"
          transform="translate(1277 461)"
          fill="#ff64cb"
        />
        <rect
          className="square-anim"
          data-name="Rectangle 8"
          rx="19"
          transform="translate(1449 461)"
          fill="#e5d5fa"
        />
        <rect
          className="square-anim"
          data-name="Rectangle 5"
          rx="19"
          transform="translate(1277 633)"
          fill="#6e00ff"
        />
        <rect
          className="square-anim"
          data-name="Rectangle 3"
          rx="19"
          transform="translate(1107 461)"
          fill="#fff"
        />
        <rect
          className="square-anim"
          data-name="Rectangle 9"
          rx="86"
          transform="translate(1107 633)"
          fill="#00f7ff"
        />
        <rect
          className="square-anim"
          data-name="Rectangle 7"
          rx="19"
          transform="translate(935 633)"
          fill="#fff"
          opacity="0.17"
        />
        <rect
          className="square-anim"
          data-name="Rectangle 4"
          rx="19"
          transform="translate(1107 805)"
          fill="#fff"
        />
      </g>
    </svg>
  );
};

export default Shapes;
