import React from "react";

import "./Home2.css";

import img1 from '../../images/icon-ux.svg'
import img2 from '../../images/icon-frontend.svg'
import img3 from '../../images/icon-logo.svg'

const Home2 = (props) => {
  return (
    <section class="skills">
      <div class="skills-container">
        <ul>
          <li class="transition2">
            <div class="icon-container one">
              <img src={img1} alt="UI/UX Icon" />
            </div>
            <p class="skill-title">UI UX Design</p>
            <p class="featured-desc skill-desc">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit.
              Asperiores, odit?
            </p>
          </li>
          <li class="transition2">
            <div class="icon-container two">
              <img src={img2} alt="UI/UX Icon" />
            </div>
            <p class="skill-title">Frontend Development</p>
            <p class="featured-desc skill-desc">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit.
              Asperiores, odit?
            </p>
          </li>
          <li class="transition2">
            <div class="icon-container three">
              <img src={img3} alt="UI/UX Icon" />
            </div>
            <p class="skill-title">Identity Design</p>
            <p class="featured-desc skill-desc">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit.
              Asperiores, odit?
            </p>
          </li>
        </ul>
      </div>
    </section>
  );
};

export default Home2;
