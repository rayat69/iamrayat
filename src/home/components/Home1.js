import React from "react";
import { Link } from 'react-router-dom';

import "./Home1.css";

import Shapes from './Shapes'

const Home1 = (props) => {
  return (
    <div class="hero">
      <div class="content">
        <h1 class="stagger1">Beautifully Crafted Web Experiences</h1>
        <div class="meet stagger1">
          <Link to='/about'>Meet Zul Ikram Musaddik</Link>
        </div>
      </div>
      <Shapes />
    </div>
  );
};

export default Home1;
