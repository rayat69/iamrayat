import React from "react";

import Home1 from "../components/Home1";
import Home2 from "../components/Home2";

const Home = (props) => {
  return (
    <React.Fragment>
      <Home1 />
      <Home2 />
    </React.Fragment>
  );
};

export default Home;
