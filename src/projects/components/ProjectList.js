import React from "react";

import './ProjectList.css'

import ProjectItems from './ProjectItems';

const ProjectList = (props) => {
  return (
    <div class="container" id="container">
      {props.items.map((item) => (
        <ProjectItems href={item.href} title={item.title} img={item.img} />
      ))}
    </div>
  );
};

export default ProjectList;
