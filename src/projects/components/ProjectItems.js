import React from "react";
import { Link } from "react-router-dom";

import './ProjectItems.css'

const ProjectItems = (props) => {
  return (
    <div className="items" id="items">
      <Link to={`projects/${props.href}`} title={props.title}>
        <div className="item" id="item">
          <div className="logo" id="logo">
            <img
              src={props.img}
              alt={props.title}
              width="100%"
              height="100%"
            />
          </div>
          <div className="name" id="name">
            <h3>{props.title}</h3>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default ProjectItems;
