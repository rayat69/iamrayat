import React from "react";

import ProjectList from "../components/ProjectList";

import stopwatch from "../projects/icons/stopwatch.svg";
import countdown from "../projects/icons/countdown.svg";
import todo from "../projects/icons/todo.svg";

const Projects = (props) => {
  const PROJECT_ITEMS = [
    {
      href: "todo-list",
      img: todo,
      title: "To-Do list",
    },
    {
      href: "stopwatch",
      img: stopwatch,
      title: "Stopwatch",
    },
    {
      href: "countdown",
      img: countdown,
      title: "Countdown Timer",
    },
  ];

  return <ProjectList items={PROJECT_ITEMS} />;
};

export default Projects;
